{
    'name': 'Ecoitec Training 1',
    'description': 'Ecoitec Training',
    'author': 'Alejandro Rubio Oliva',
    'depends': ['contacts', 'sale', 'delivery'],
    'category': 'Uncategorized',
    'installable': True,
    'data': [
        'views/partner_view.xml',
        'views/order_view.xml',
        'wizard/message_view.xml',
        'wizard/cancel_view.xml'
    ],
}
