from odoo import fields, models, api, _
from odoo.exceptions import ValidationError


class Cancel(models.TransientModel):
    _name = 'ecoitec_training_1.cancel'
    cancel_reason = fields.Char('Reason for cancellation', related='sale_order_id.cancel_reason',
                                store=True, readonly=False)
    sale_order_id = fields.Many2one('sale.order')

    @api.multi
    def cancel(self):
        """Save cancellation"""
        if self.cancel_reason:
            self.sale_order_id.action_cancel()
            return {'type': 'ir.actions.act_window_close'}
        raise ValidationError(_('Fill in the reason for cancellation'))

    @api.multi
    def exit(self):
        """Exit cancellation"""
        return {'type': 'ir.actions.act_window_close'}
