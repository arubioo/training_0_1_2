from odoo import fields, models, api


class Message(models.TransientModel):
    _name = 'ecoitec_training_1.message'
    message = fields.Html(required=True)

    @api.multi
    def close(self):
        """Close wizard"""
        return {'type': 'ir.actions.act_window_close'}
