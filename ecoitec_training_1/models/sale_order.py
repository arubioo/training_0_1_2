from odoo import fields, models, api, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    favorite_color = fields.Char('Favorite color (Customer)', related='partner_id.favorite_color', help="")
    second_salesperson = fields.Many2one('res.users', 'Second salesperson')
    cancel_reason = fields.Char()

    @api.onchange('partner_id')
    def onchange_client_order_ref(self):
        if self.second_salesperson != self.partner_id.second_salesperson:
            self.second_salesperson = self.partner_id.second_salesperson
            return {
                'warning': {
                    'title': _('Change of second salesperson'),
                    'message': _('The second salesperson has been changed.')
                }
            }

    @api.multi
    def action_confirm(self):
        super().action_confirm()
        delivery_qty = self.env['sale.order'].search_count(
            [('user_id', '=', self.env.uid), ('state', 'in', ('sale', 'done'))]) + 1

        return {
            'name': _('Order placed successfully'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'ecoitec_training_1.message',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {
                'default_message': _(f'<h3>Congratulations on your order number {delivery_qty}</h3>')
            }
        }

    @api.multi
    def my_action_cancel(self):
        return {
            'name': _('Cancellation assistant'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'ecoitec_training_1.cancel',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {
                'default_sale_order_id': self.id
            }
        }
