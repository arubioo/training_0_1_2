from odoo import api, fields, models
from datetime import date
from dateutil.relativedelta import relativedelta


class Partner(models.Model):
    _inherit = 'res.partner'

    favorite_color = fields.Char('Favorite color')
    date_of_birth = fields.Date('Date of birth')
    second_salesperson = fields.Many2one('res.users', 'Second salesperson')
    info = fields.Html('Customer info')
    years = fields.Integer('Years', store=True, compute="_compute_years")

    @api.one
    @api.depends('date_of_birth')
    def _compute_years(self):
        self.years = relativedelta(date.today(), self.date_of_birth).years


    # @api.multi
    # @api.depends('date_of_birth')
    # def _compute_years(self):
    #     for record in self:
    #         record.years = relativedelta(date.today(), record.date_of_birth).years
