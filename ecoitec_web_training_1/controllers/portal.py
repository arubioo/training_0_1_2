import pdb

import phonenumbers
import re
from odoo import _
from odoo.http import request
from odoo.addons.portal.controllers.portal import CustomerPortal


class TrainingCustomerPortal(CustomerPortal):
    def details_form_validate(self, data):
        error, error_message = super().details_form_validate(data)

        if data.get('state_id') and request.env['res.city'].search([('country_id', '=', int(data.get('country_id')))]):
            if not request.env['res.city'].search(
                    [('name', '=ilike', data.get('city')), ('state_id', '=', int(data.get('state_id')))]):
                error["city"] = 'error'
                error_message.append(_('Invalid City! This city does not correspond to the selected state.'))

        phone = data.get('phone')
        if re.search(r"^([+]|\d)\d{1,14}$", phone):
            if '+' in phone:
                phone = phonenumbers.parse(phone)
                phone_country = request.env['res.country'].search([('phone_code', '=', phone.country_code)])
                if phone_country.id != int(data.get('country_id')):
                    error["phone"] = 'error'
                    error_message.append(_('Invalid Phone! Phone prefix does not match the country code.'))
        else:
            error["phone"] = 'error'
            error_message.append(_('Invalid Phone!'))

        if data.get('country_id') and int(data.get('country_id')) == 68:
            if re.search(r"\D", data.get('zipcode')):
                error["zipcode"] = 'error'
                error_message.append(_('Invalid Zipcode! In Spain the postal code can only contain numbers.'))

        if data.get('zipcode') not in [zipcode.name for zipcode in request.env['res.city'].search(
                [('name', '=ilike', data.get('city'))]).zip_ids]:
            error["zipcode"] = 'error'
            error_message.append(_('Invalid Zipcode! This zipcode does not correspond to the selected city'))

        return error, error_message
