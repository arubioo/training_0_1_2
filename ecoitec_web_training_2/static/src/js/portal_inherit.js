odoo.define('portal.portal', function (require) {
    'use strict';
    require('web.dom_ready');

    if ($('.o_portal_details').length) {
        var region_options = $("select[name='region_id']:enabled option:not(:first)");
        $('.o_portal_details').on('change', "select[name='country_id']", function () {
            var select = $("select[name='region_id']");
            region_options.detach();
            var displayed_region = region_options.filter("[data-country_id=" + ($(this).val() || 0) + "]");
            var nb = displayed_region.appendTo(select).show().size();
            select.parent().toggle(nb >= 1);
        });
        $('.o_portal_details').find("select[name='country_id']").change();
    }

    if ($('.o_portal_details').length) {
        var state_options = $("select[name='state_id']:enabled option:not(:first)");
        $('.o_portal_details').on('change', "select[name='region_id']", function () {
            var select = $("select[name='state_id']");
            state_options.detach();
            var displayed_state = state_options.filter("[data-region_id=" + ($(this).val() || 0) + "]");
            var nb = displayed_state.appendTo(select).show().size();
            select.parent().toggle(nb >= 1);
        });
        $('.o_portal_details').find("select[name='region_id']").change();
    }
});