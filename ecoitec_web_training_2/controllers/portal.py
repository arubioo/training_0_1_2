from odoo.addons.portal.controllers.portal import CustomerPortal
from odoo.http import content_disposition, Controller, request, route


class Portal(CustomerPortal):
    MANDATORY_BILLING_FIELDS = ["name", "phone", "email", "street", "city", "country_id", "region_id"]

    def _prepare_portal_layout_values(self):
        response = super()._prepare_portal_layout_values()
        regions = request.env['res.country.region'].sudo().search([])

        response.update({
            'regions': regions
        })
        return response
