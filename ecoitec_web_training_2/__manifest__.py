{
    'name': 'Ecoitec Web Training 2',
    'description': 'Ecoitec Web Training 2',
    'author': 'Alejandro Rubio Oliva',
    'depends': ['website', 'ecoitec_web_training_1'],
    'category': 'Uncategorized',
    'installable': True,
    'data': [
        'views/portal_my_details_inherit.xml'
    ],
}
